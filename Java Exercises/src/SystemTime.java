/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.*;
import java.text.SimpleDateFormat;

public class SystemTime {
    public static void main (String [] args){
        Date date = new Date();

        String displayDate = new SimpleDateFormat("d MMMMM yyyy").format(date);
        String displayTime = new SimpleDateFormat("HH:mm a").format(date);

        System.out.print("Current Date: " + displayDate + "\n");
        System.out.println("Current Time: " + displayTime);
    }
}
