/**
 * Created by Elvis on 28/Sep/2015.
 */
import java.io.*;
import java.nio.file.Files;

public class DeleteFile {
    public static void main(String [] args){

        try {
            File file = new File("/docs/input.txt");
           if( file.delete()){
               System.out.println("Successfully deleted file: " + file.getName());
           }else {
               System.out.println("Failed to delete file: " + file.getName());
           }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
