import jdk.nashorn.internal.codegen.CompilerConstants;

/**
 * Created by Elvis on 25/Sep/2015.
 */
public class CallingMethods {
    public static void main(String [] args){
        String name = "", surname = "";
        CallingMethods objCallingMethods = new CallingMethods();

        noArguments();
        fixedArguments(name, surname);
        System.out.println("I intern at: " + returnValue());
        displayPrivate();

        objCallingMethods.optionalArguments("Inforware", "Studios");
        objCallingMethods.optionalArguments("Java programming");

        System.out.println();
        EmailAddress objEmail = new EmailAddress();
        System.out.println(objEmail.email); //Pass by value

        otherEmail(objEmail);
        System.out.println(objEmail.email); //Pass by reference
    }
    public static void noArguments(){
        System.out.println("This is a method with no arguments");
    }
    public static void fixedArguments(String name, String surname){
        name = "Elvis";
        surname = "Mpai";
        System.out.println("My name is: " + name);
        System.out.println("My surname is: " + surname);
    }
    public static void optionalArguments (String lineOne, String lineTwo){
        System.out.println(lineOne + " " + lineTwo);
    }
    public static void optionalArguments (String lineThree){
        System.out.println(lineThree);
    }
    public static String returnValue (){
        String company = "Digital Geekaship";
        return company;
    }
    public static void otherEmail(EmailAddress mail){
        mail.email = "elvismpai@ymail.com";
    }
    private static void displayPrivate(){
        System.out.println("This is a private method");
        displayProtected();
    }
    protected static void displayProtected(){
        System.out.println("This is a protected method\n");
    }
}
class EmailAddress{
    public String email = "androidxero@gmail.com";
}
