/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.Scanner;

public class SumOfDigits {
    public static void main (String [] args){
        String digits;
        int sumOfNumbers = 0;
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a string of numbers: ");
        digits = in.next();

        for(int loopDigits = 0; loopDigits < digits.length(); loopDigits++){
            String numbers = "" + digits.charAt(loopDigits);
            sumOfNumbers += Integer.parseInt(numbers);
        }
        System.out.println("Sum of digits is: " + sumOfNumbers);

    }
}
