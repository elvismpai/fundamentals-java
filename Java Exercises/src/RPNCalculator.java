/**
 * Created by Elvis on 07/Oct/2015.
 */
import java.io.*;
import java.util.*;

public class RPNCalculator {
    public static void main(String[] args) throws IOException {
        System.out.println("Enter expression: ");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        for (; ; ) {
            String string = in.readLine();
            if (string == null)
                break;
            Stack<String> stack = new Stack<String>();
            stack.addAll(Arrays.asList(string.trim().split("[ \t]+")));
            if (stack.peek().equals("")) continue;
            try {
                double r = evalrpn(stack);
                if (!stack.empty()) throw new Exception();

                System.out.println("Answer : " + r);
            } catch (Exception e) {
                System.out.println("ERROR! Enter again ");
            }
        }
    }

    private static double evalrpn(Stack<String> stack) throws Exception {
        String tk = stack.pop();
        double x, y;
        try {
            x = Double.parseDouble(tk);
        } catch (Exception e) {
            y = evalrpn(stack);
            x = evalrpn(stack);
            if (tk.equals("+")) x += y;
            else if (tk.equals("-")) x -= y;
            else if (tk.equals("*")) x *= y;
            else if (tk.equals("/")) x /= y;
            else throw new Exception();
        }
        return x;
    }
}
