/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.*;

public class TimesTable {
    public static void main(String [] args){
        Scanner in = new Scanner(System.in);

        System.out.println("Multiple table: 12 x 12: ");
         for (int loopMultiple = 1;loopMultiple < 13; loopMultiple++){
             for(int loopTable = 1; loopTable < 13; loopTable++){
                 System.out.print(loopMultiple + " x " + loopTable + " = ");
                 System.out.print(loopMultiple * loopTable + "\n");
             }
             System.out.print("\n");
         }
        /*int multiple, table = 13;

        System.out.print("Enter a multiple you want to display: "); //Requesting user input
        multiple = in.nextInt();

        for(int loopTable = 1; loopTable < table; loopTable++){
            System.out.print(multiple + " x " + loopTable + " = ");
            System.out.print(loopTable * multiple + "\n");

        }*/
    }
}
