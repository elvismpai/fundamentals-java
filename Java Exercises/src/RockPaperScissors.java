/**
 * Created by Elvis on 09/Oct/2015.
 */

import java.util.*;
import java.io.*;

public class RockPaperScissors {
    public static final int rock = 1, paper = 2, scissor = 3;
    public static String path = "C:\\Users\\DITLOU_DIKGOMO\\Documents\\1InfowareDocuments\\JavaFundamentals\\fundamentals-java\\Java Exercises\\src\\playerData.txt";

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int countGames = 0, exitGame = 0;

        System.out.println("PLAY: ROCK_PAPER_SCISSORS \nEnter (ZERO) to end game.");
        System.out.print("Enter a number: Rock is (1)/Paper is (2)/Scissors is (3): ");
        int input = sc.nextInt();

        if (input == rock || input == paper || input == scissor || input == exitGame) {
            while (input != 0) {
                savePlayerData(input);
                checkWinner(input, computerChoice());
                System.out.println();
                System.out.print("\nRock (1) //Paper (2) //Scissors (3): ");
                input = sc.nextInt();
                countGames++;
                if (input == exitGame) {
                    System.out.println("\nYou have played " + countGames + " games");
                    System.out.println("Thanks for playing ROCK-PAPER-SCISSORS.");
                }
            }
        } else {

            System.out.print("ERROR: Invalid input, enter Rock(1),Paper(2) or Scissors(3)");
        }
    }

    public static void savePlayerData(int data) {
        File file = new File(path);
        try {
            if (!file.exists())
                file.createNewFile();

            FileWriter write = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(write);
            bw.write(data + "\n");
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int computerChoice() {
        Random rand = new Random();
        int computerChoice = 0;
        File file = new File(path);
        String line = "";
        int[] fileData = new int[100];
        int countData = 0, countRock = 0, countPaper = 0, countScissor = 0;

        try {
            FileReader fr = new FileReader(path);
            BufferedReader br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                fileData[countData] = Integer.parseInt(line);
                countData++;
            }
            br.close();
            if(countData <= 10)
                computerChoice = 1 + rand.nextInt(3);
            else{
                for(int i = 0; i < fileData.length; i++){
                    if(fileData[i] == rock)
                        countRock++;
                    else if(fileData[i] == paper)
                        countPaper++;
                    else if(fileData[i] == scissor)
                        countScissor++;
                }
                if(countPaper > countRock && countPaper > countScissor)
                    computerChoice = scissor;
                else if(countRock > countScissor && countRock > countPaper)
                    computerChoice = paper;
                else if(countScissor > countPaper && countScissor > countRock)
                    computerChoice = rock;
            }

        } catch (IOException e) {
            System.out.print("ERROR: File not found.");
        }
        return computerChoice;
    }

    public static void checkWinner(int player, int computer) {
        if (player == rock && computer == scissor)
            System.out.print("You have WON - Rock beats Scissors.");
        else if (player == paper && computer == scissor)
            System.out.print("You have LOST - Scissors beat Paper.");
        else if (player == scissor && computer == paper)
            System.out.print("You have WON - Scissors beat Paper.");
        else if (player == rock && computer == paper)
            System.out.print("You have LOST - Paper beats Rock.");
        else if (player == paper && computer == rock)
            System.out.print("You have WON - Paper beats Rock.");
        else if (player == scissor && computer == rock)
            System.out.print("You have LOST - Rock beats Scissors.");
        else if (player == computer)
            System.out.print("DRAW");
    }
}
