/**
 * Created by Elvis on 23/Sep/2015.
 */

import java.util.*;

public class QuickSort {
    public static void main(String[] args) {
        int[] arrayNumbers = {30, 4, 1, 25, 5, 3, 20, 10, 2, 15};
        int[] less = new int[10];
        int[] equal = new int[10];
        int[] greater = new int[10];
        int pivot = 0;

        if (arrayNumbers.length > 1) {
            pivot = arrayNumbers[5];
            for (int loopArray = 0; loopArray < arrayNumbers.length; loopArray++) {
                if (arrayNumbers[loopArray] < pivot)
                    less[loopArray] = arrayNumbers[loopArray];

                if (arrayNumbers[loopArray] == pivot)
                    equal[loopArray] = arrayNumbers[loopArray];

                if (arrayNumbers[loopArray] > pivot)
                    greater[loopArray] = arrayNumbers[loopArray];

            }
        }
        sort(less);
        sort(greater);

        System.out.print("Original array: ");
        for (int originalArray : arrayNumbers) {
            System.out.print(originalArray + " ");
        }

        System.out.print("\nQuick sort: ");

        for (int x = 0; x < 10; x++) {
            if (less[x] == 0) {                        //This if statement is for removing zeros in the array
            } else
                System.out.print(less[x] + " ");
        }
        System.out.print(pivot + " ");

        for (int x = 0; x < 10; x++)
            if (greater[x] == 0) {
            } else
                System.out.print(greater[x] + " ");

    }

    public static int sort(int[] quickArray) {
        int temp = 0;
        for (int firstArray = 0; firstArray < 10; firstArray++) {
            for (int secondArray = 1; secondArray < (10 - firstArray); secondArray++) {
                if (quickArray[secondArray - 1] > quickArray[secondArray]) {
                    temp = quickArray[secondArray - 1];
                    quickArray[secondArray - 1] = quickArray[secondArray];
                    quickArray[secondArray] = temp;

                }
            }
        }
        return temp;

    }
}

