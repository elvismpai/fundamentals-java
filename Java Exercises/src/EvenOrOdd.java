/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.Scanner;

public class EvenOrOdd {
    public static void main(String [] args){
        Scanner in = new Scanner(System.in);

        System.out.println("Enter a number to check: Even Or Odd");
        int number = in.nextInt();

        if (number % 2 == 0)
            System.out.println("The number: "+ number +" is an EVEN number");
        else
            System.out.println("The number: "+ number +" is an ODD number");

    }
}
