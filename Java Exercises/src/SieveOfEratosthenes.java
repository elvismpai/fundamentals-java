/**
 * Created by Elvis on 06/Oct/2015.
 */
import java.util.*;

public class SieveOfEratosthenes {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int nNumber;
        System.out.print("Enter a prime number: ");
        nNumber = sc.nextInt();

        boolean[] primeNumbers = isPrimeNumber(nNumber);

        for(int x = 0; x < nNumber; x++){
            if(primeNumbers[x]){
                System.out.print(x +" ");
            }
        }
    }

    public static boolean[] isPrimeNumber(int max) {
        boolean[] primeNumber = new boolean[max];
        for (int x = 2; x < max; x++) {
            primeNumber[x] = true;
        }
        for (int x = 2; x < max; x++) {

            if (primeNumber[x]) {
                for (int y = x + x; y < max; y = y + x) {
                    primeNumber[y] = false;
                }
            }
        }
        return primeNumber;
    }
}
