/**
 * Created by Elvis on 02/Oct/2015.
 */

import java.util.*;

public class CommasAndAnds {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        Scanner sc = new Scanner(System.in);
        String[] commaWord = {};
        String word = "", newWord = "", replaceBrackets = "";
        int count = 0, countComma = 0;
        String newString = "", finalString = "";

        System.out.print("Enter your string: ");
        word = sc.next();

        while (count < word.length()) {
            replaceBrackets = word.replace('[', '{');
            newWord = replaceBrackets.replace(']', '}');
            count++;
        }

        if (word.length() == 2)
            System.out.print(newWord + " # (No input words).");
        else {
            if (newWord.contains("" + "\""))
                commaWord = newWord.split("\"");

            for (int i = 0; i < commaWord.length; i++)
                newString += commaWord[i];

            for (int i = 0; i < newString.length(); i++) {
                if (newString.contains("" + ",")) {
                    countComma++;
                    if(countComma == 1) {
                        sb = new StringBuilder(newString);
                        int index = newString.indexOf(",");
                        sb.deleteCharAt(index);
                        sb.insert(index, " and ");
                    }else{
                        for(int x = newString.length(); x >= 0; x--){
                            sb = new StringBuilder(newString);
                            int index = newString.indexOf(",");
                            sb.deleteCharAt(index);
                            sb.insert(index, " and ");
                        }
                    }
                }
            }
            System.out.print("Final Output: " + sb);
        }
    }
}
