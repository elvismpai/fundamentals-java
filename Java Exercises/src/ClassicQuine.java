/**
 * Created by Elvis on 28/Sep/2015.
 */
public class ClassicQuine {
    public static void main(String[] args) {
        String f = "class ClassicQuine{\n"+
                    "       public static void main(String[] args){\n" +
                    "               String f = %c%s%1$c;\n" +
                    "             System.out.printf(f, 34 ,f);\n" +
                    "       }\n" +
                    "}";
        System.out.printf(f, 34, f);
    }
}
