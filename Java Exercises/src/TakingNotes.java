/**
 * Created by Elvis on 07/Oct/2015.
 */

import java.io.*;
import java.nio.channels.*;
import java.util.Date;

public class TakingNotes {
    public static void main(String[] args) throws IOException {
        String file = "C:/Users/DITLOU_DIKGOMO/Documents/1InfowareDocuments/JavaFundamentals/fundamentals-java/Java Exercises/src/notes.txt";

        if (args.length > 0) {
            PrintStream ps = new PrintStream(new FileOutputStream(file, true));
            ps.print("\t" + args[0]);

            for (int i = 1; i < args.length; i++)
                ps.print(" " + args[i]);

            ps.println(new Date());
            ps.println();
            ps.close();
        } else {
            FileChannel fc = new FileInputStream(file).getChannel();
            fc.transferTo(0, fc.size(), Channels.newChannel(System.out));
            fc.close();
        }
    }
}
