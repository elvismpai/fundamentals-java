/**
 * Created by Elvis on 09/Oct/2015.
 */
import java.util.*;

public class WarmUpExercise {
    public static void main(String args[]) {
        System.out.print("The first element in selected list is: ");
        fruits(random());
    }

    public static void fruits(double weight) {
        double[] weights = {0.25, 0.5, 0.2, 0.05};
        String[] fruit = {"Apple", "Pear", "Grape", "Orange"};
        double sum = 0;
        double[] accumulativeSum = new double[weights.length];

        for (int i = 0; i < weights.length; i++) {
            sum += weights[i];
            accumulativeSum[i] = sum;
        }
        for (int element = 0; element < accumulativeSum.length; element++) {
            if (accumulativeSum[element] >= weight) {
                System.out.print(fruit[element]);
                break;
            }
        }
    }

    public static double random() {
        Random rand = new Random();
        double randomNumber = rand.nextInt(100) / 100d;
        return randomNumber;
    }
}
