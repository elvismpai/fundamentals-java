/**
 * Created by Elvis on 01/Oct/2015.
 */

import java.util.*;

public class PigLatin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        final String vowels = "aeiouAEIOU";
        String hyphen = "-";
        int choose = 0;

        System.out.println("Enter (1) to covert ENGLISH to PIG LATIN");
        System.out.println("Enter (2) to covert PIG LATIN to ENGLISH");
        choose = sc.nextInt();

        if (choose > 1 || choose < 1)
            choose = 1;
        if (choose == 1) {
            System.out.print("Enter word - ENGLISH to PIG LATIN: ");
            String word = sc.nextLine();

            String beforeVowel = "";
            int cut = 0;

            while (cut < word.length() && !vowels.contains("" + word.charAt(cut))) {
                beforeVowel += word.charAt(cut);
                cut++;
            }
            if (cut != 0) {
                System.out.println(word.substring(cut) + hyphen + beforeVowel + "ay");
            } else
                System.out.println(word + hyphen + "way");

        }else if(choose == 2) {

            System.out.print("Enter word - PIG LATIN to ENGLISH: ");
            String word = sc.nextLine();

            String beforeHyphen = "", afterHyphen = "";
            int cut = 0, afterCut = 0;

            while (cut < word.length() && !hyphen.contains("" + word.charAt(cut))) {
                beforeHyphen += word.charAt(cut);
                cut++;
            }

            if (cut > 0) {
                afterHyphen = word.substring(cut + 1);
                System.out.println(afterHyphen.substring(0, afterHyphen.length() - 2) + beforeHyphen);
            } else
                System.out.println(beforeHyphen);
        }

    }
}
