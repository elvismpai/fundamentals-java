/**
 * Created by Elvis on 21/Sep/2015.
 */
public class BubbleSort {
    public static void main(String [] args){
        int[] arrayOfNum = {5,25,30,20,2,15,3,10,1};
        int temp = 0, arrayLength = arrayOfNum.length;

        System.out.print("Numbers before bubble sort: ");
        for(int arrayDisplay : arrayOfNum){
            System.out.print(arrayDisplay + " ");
        }
        for (int firstArray = 0; firstArray < arrayLength; firstArray ++){
            for(int secondArray = 1; secondArray < (arrayLength - firstArray); secondArray ++){
                if(arrayOfNum[secondArray - 1] > arrayOfNum[secondArray]){
                    temp = arrayOfNum[secondArray - 1];
                    arrayOfNum[secondArray - 1] = arrayOfNum[secondArray];
                    arrayOfNum[secondArray] = temp;

                }
            }
        }

        System.out.print("\n");
        System.out.print("Numbers after bubble sort: ");
        for (int arrayDisplay : arrayOfNum){
            System.out.print(arrayDisplay + " ");
        }

    }
}
