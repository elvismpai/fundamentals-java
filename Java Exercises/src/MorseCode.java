/**
 * Created by Elvis on 05/Oct/2015.
 */
import java.util.*;
import javax.swing.*;

public class MorseCode {
    public static StringBuilder toMorseCode;
    public static StringBuilder toNormalString;
    public static String morseCoding[] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.--", ".-..", "--", "-.", "---", ".--.", "-- .-", ".-.", "...", "-", "--.", "...-", ".--", "-..-", "-.--", "--..", ".----", "..---", "...--", "....-", ".....", " -....", "--...", "---..", "----.", "-----","  "};
    public static char normalString1[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',' '};
    public static boolean toNormal;
    public static boolean toNormal2 = true;

    public static void main(String args[]) {
        String input = JOptionPane.showInputDialog("Enter 1 if you are converting to morse code or if converting from morse code to normal please enter 2  ");

        do {
            if (input.equals("1")) {
                toNormal = true;
                String word = JOptionPane.showInputDialog("Enter word to be converted  ");
                String toMorse = toMorseCode(word.toUpperCase());

                System.out.println(toMorse);
            } else if (input.equals("2")) {
                toNormal = true;
                String word = JOptionPane.showInputDialog("Enter Morse Code to be converted  ");
                char toString = toNormalString(word);
                if (toString != ' ') {
                    System.out.print(toString);
                    String word101 = JOptionPane.showInputDialog("Would like to convert another moscode,if yes click (y) or (n) if you done converting");
                    if (word101.equalsIgnoreCase("y")) {
                        do {

                            String word202 = JOptionPane.showInputDialog("Enter another moscode to be converted");
                            toString = toNormalString(word202);
                            System.out.print(toString);
                            word101 = JOptionPane.showInputDialog("Would like to convert another moscode,if yes click (y) or (n) if you done converting");
                        }
                        while (word101.equalsIgnoreCase("y"));
                    } else {
                        System.exit(0);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "This word does not exist");
                    System.exit(0);
                }
            } else {
                toNormal = false;
                JOptionPane.showMessageDialog(null, " Please Enter 1 or 2 only,  " + input + " is not allowed");
                input = JOptionPane.showInputDialog("Enter 1 if you are converting to morse code or if converting from morse code to normal please enter 2  ");
            }
        }
        while (toNormal != toNormal2);
    }

    public static String toMorseCode(String normalString) {
        StringBuilder newWord = new StringBuilder();
        String newString = "";
        for (int x = 0; x < normalString.length(); x++) {
            for (int numberOfElemtsMoscodeArray = 0; numberOfElemtsMoscodeArray < normalString1.length; numberOfElemtsMoscodeArray++) {

                if (normalString1[numberOfElemtsMoscodeArray] == normalString.charAt(x) ) {
                    String empty = " ";
                    newWord.append(morseCoding[numberOfElemtsMoscodeArray] + empty  );
                    newString = newWord.toString();
                }
            }
        }
        return newString;
    }

    public static char toNormalString(String morseString) {
        //StringBuilder newWord = new StringBuilder();
        char newString = ' ';
        for (int numberOfElemtsMoscodearray = 0; numberOfElemtsMoscodearray < morseCoding.length; numberOfElemtsMoscodearray++) {
            if (morseCoding[numberOfElemtsMoscodearray].equals(morseString)) {

                newString = normalString1[numberOfElemtsMoscodearray];
            }
        }
        return newString;
    }

}
