/**
 * Created by Elvis on 07/Oct/2015.
 */
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class WalkTree {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter path to search: ");
        String path = sc.nextLine();
        System.out.print("Enter extension to disaply: ");
        String extension = sc.nextLine();
        System.out.println("\nFiles with extension ("+ extension + "):");

        File dir = new File(path);
        File listFile[] = dir.listFiles();
        for (File files : listFile) {
            if(files.getName().endsWith(extension))
                System.out.println(files.getName());
        }
    }
}

