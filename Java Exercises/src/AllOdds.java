/**
 * Created by Elvis on 28/Sep/2015.
 */
public class AllOdds {
    public static void main(String[] args){

        System.out.println(printOdds());
    }
    public static String printOdds(){
        String oddNumbers = "";
        System.out.println("Odd numbers: ");
        for(int i = 1; i < 100; i++){
            if(i % 2 != 0)
                oddNumbers += i + " ";
        }
        return oddNumbers;
    }
}
