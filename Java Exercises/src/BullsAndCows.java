/**
 * Created by Elvis on 22/Sep/2015.
 */

import java.util.*;

public class BullsAndCows {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random randomGenerator = new Random();
        int[] randomNumbers = new int[4];
        int[] guessNumbers = new int[4];
        int maxLoop = 4, countCow = 0, countBull = 0, cows = 0;

        for (int i = 0; i < randomNumbers.length; i++)
            randomNumbers[i] = 1 + randomGenerator.nextInt(9);


        System.out.println("Generating...Done");
        System.out.println("PLAY - BULLS AND COWS\n");
        System.out.print("Enter FOUR (4) numbers: ");
        int numbers = in.nextInt();
        System.out.println("----------------------------");

        String stringNumbers = Integer.toString(numbers);

        for (int x = 0; x < maxLoop; x++) {
            guessNumbers[x] = Integer.parseInt(stringNumbers.split("")[x]);
        }

        for (int checkBull = 0; checkBull < maxLoop; checkBull++) {
            for (int checkCow = 0; checkCow < maxLoop; checkCow++) {
                if (guessNumbers[checkBull] == randomNumbers[checkCow]) {
                    countCow++;
                }
            }
            if (guessNumbers[checkBull] == randomNumbers[checkBull]) {
                countBull++;
            }
            if (countBull >= 1)
                cows = countCow - countBull;

        }
        checkDuplicate(countBull, cows, randomNumbers, guessNumbers);
    }

    public static void checkDuplicate(int cow, int bull, int[] random, int[] numbers) {
        boolean bool = false;
        for (int x = 1; x < 4; x++) {
            if (numbers[0] == numbers[x])
                bool = true;

            else if (numbers[1] == numbers[2])
                bool = true;

            else if (numbers[2] == numbers[3])
                bool = true;

            else if (numbers[1] == numbers[3])
                bool = true;

        }
        if (!(bool)) {
            checkForZero(cow, bull, random, numbers);
        } else {
            System.out.println("ERROR 01: You have entered duplicate numbers");
            System.out.println("TRY AGAIN");
        }
    }

    public static void checkForZero(int cow, int bull, int[] random, int[] numbers) {
        boolean bool = false;
        for (int i = 0; i < 4; i++) {
            if (numbers[i] == 0) {
                bool = true;
            }
        }
        if (!(bool)) {
            displayResults(cow, bull, random, numbers);
        } else {
            System.out.println("ERROR 02: Your number contains a zero");
            System.out.println("TRY AGAIN");
        }
    }

    public static void displayResults(int cow, int bull, int[] random, int[] numbers) {
        System.out.println("\nYou have got " + cow + " BULL/S");
        System.out.println("You have got " + bull + " COW/S\n");

        System.out.print("The random numbers are: ");
        for (int i = 0; i < 4; i++)
            System.out.print(random[i]);
        System.out.print("\n");
        winOrLose(random, numbers);
    }

    public static void winOrLose(int[] random, int[] numbers) {
        int correctRandom = 0, correctNumbers = 0;

        for (int x = 0; x < 4; x++) {
            correctRandom = random[x];
            correctNumbers = numbers[x];
        }
        if (correctNumbers == correctRandom)
            System.out.println("Congratulations! You have won BULLS AND COWS.");
        else
            System.out.println("Sorry, you haven't matched the numbers \n" + "TRY AGAIN");
    }
}
