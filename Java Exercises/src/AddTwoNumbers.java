/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.*;

public class AddTwoNumbers {
    public static void main(String [] arg){
        Scanner in = new Scanner(System.in);

        System.out.print("Enter two numbers separated by space: ");

        int firstNumber= in.nextInt(),secondNumber=in.nextInt();
        int sum = firstNumber + secondNumber;

        System.out.print(firstNumber+" "+secondNumber+" "+sum);
    }
}