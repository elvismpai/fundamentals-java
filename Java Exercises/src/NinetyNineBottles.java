/**
 * Created by Elvis on 21/Sep/2015.
 */
public class NinetyNineBottles {
    public static void main (String [] args){

        for(int bottles = 100; bottles > 0; bottles--){
            System.out.print(bottles - 1 + " bottles of beer on the wall"+"\n");
            if(bottles < 2) {
                break;
            }else{
                System.out.print(bottles - 1 + " bottles of beer" + "\n");
                System.out.print("Take one down, pass it around" + "\n");
            }

        }

    }

}
