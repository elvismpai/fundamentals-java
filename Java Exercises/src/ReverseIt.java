/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.regex.*;

public class ReverseIt {
    public static void main(String [] args) {
        String myString = "Intern at Inforware Studios";
        String[] arrayString = {"E", "L", "V", "I", "S"};
        String ReversedString = " ";
        String reversedArray = " ";

        System.out.print("Original String: " + myString + "\n");
        System.out.print("Reversed string: ");
        for (int loopString = myString.length() - 1; loopString >= 0; loopString--) {
            ReversedString = ReversedString + myString.charAt(loopString);
        }
        System.out.print(ReversedString + "\n\n");

        System.out.print("Original Array: ");
        for (String loopArrayOfWords : arrayString)
            System.out.print(loopArrayOfWords);
        
        System.out.print("\nReversed Array: ");
        for(int loopArray = 4; loopArray >= 0; loopArray--){
            reversedArray = reversedArray + arrayString[loopArray];
        }
        System.out.print(reversedArray);
    }
}
