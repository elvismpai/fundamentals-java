/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.*;

public class FindMaxNumber {
    public static void main(String [] args){
        Scanner in = new Scanner((System.in));
        int[] arrayOfNumbers = new int[5];
        int maxNumber = 0;

        System.out.println("Enter 5 numbers of numbers: ");
        for(int loopEnterNumbers = 0; loopEnterNumbers < 5; loopEnterNumbers++){
            arrayOfNumbers[loopEnterNumbers] = in.nextInt();
        }
        System.out.print("The numbers you have entered: ");
        for(int displayArray : arrayOfNumbers){
            System.out.print(displayArray + " ");
        }
        for(int loopMax : arrayOfNumbers){
            if(maxNumber < loopMax){
               maxNumber = loopMax;
            }
        }
        System.out.print("\nThe maximum number in the array is: " + maxNumber);
    }
}
