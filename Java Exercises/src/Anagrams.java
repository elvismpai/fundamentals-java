import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by Elvis on 9/18/2015.
 */
public class Anagrams {
    public static void main(String[] args) {
        try {
            URL myUrl = new URL("http://www.puzzlers.org/pub/wordlists/unixdict.txt");
            InputStreamReader reader = new InputStreamReader(myUrl.openStream());
            BufferedReader br = new BufferedReader(reader);
            Scanner scan = new Scanner(System.in);

            String anagrams = "";
            System.out.print("Please enter word: ");
            String word = scan.next();
            
            while ((anagrams = br.readLine()) != null) {
                if (word.length() == anagrams.length()) {
                    String inputWord = sortCharacter(word);
                    String anotherString = sortCharacter(anagrams);

                    if (inputWord.equalsIgnoreCase(anotherString)) {
                        System.out.println(anagrams);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("ERROR: Failed");
        }
    }
    public static String sortCharacter(String value) {

        char[] charArray = value.toCharArray();
        Arrays.sort(charArray);
        String valueToReturn = String.valueOf(charArray);
        return valueToReturn;
    }
}
