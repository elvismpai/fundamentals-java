/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.*;
import java.io.*;

public class AlignColumns {
    public static void main(String[] args) {
        String[] information = {"Given$a$text$file$of$many$lines,$where$fields$within$a$line",
                "are$delineated$by$a$single$'dollar'$character,$write$a$program",
                "that$aligns$each$column$of$fields$by$ensuring$that$words$in$each",
                "column$are$separated$by$at$least$one$space.",
                "Further,$allow$for$each$word$in$a$column$to$be$either$left",
                "justified,$right$justified,$or$center$justified$within$its$column."};

        String[] splitString = null;
        String message = "";
        System.out.println("Left Justified Column");
        for (String info : information) {
            splitString = info.split("\\$");

            for (String i : splitString) {
                System.out.printf("%-15s", i);
            }
            System.out.println();
        }

        System.out.println("\nRight Justified Column");
        for (String info : information) {
            splitString = info.split("\\$");

            for (String j : splitString) {
                System.out.printf("%15s", j);

            }
            System.out.println();
        }
        System.out.println("\nCenter Justified Column");

        for (String info : information) {
            splitString = info.split("\\$");

            for (String k : splitString) {
                int space = k.length() / 2;
                System.out.printf("%s", k);
            }
            System.out.println();
        }
    }
}

