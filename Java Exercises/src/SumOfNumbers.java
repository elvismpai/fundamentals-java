/**
 * Created by Elvis on 21/Sep/2015.
 */
public class SumOfNumbers {
    public static void main (String [] args){
        int[] arrayOfNumbers = {58,2,4,6,12,6,25};
        int sumOfNumbers = 0;

        for(int calSum : arrayOfNumbers){
            sumOfNumbers += calSum;
        }

        System.out.print("The sum of the array is: " + sumOfNumbers);
    }
}
