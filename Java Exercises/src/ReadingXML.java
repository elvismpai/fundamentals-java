/**
 * Created by Elvis on 28/Sep/2015.
 */
import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;

public class ReadingXML {
    public static void main(String args []){

        try{
            File xmlFile = new File("C:/Users/DITLOU_DIKGOMO/Documents/1InfowareDocuments/JavaFundamentals/fundamentals-java/Java Exercises/src/input.xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("Student");

            for(int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    System.out.println(eElement.getAttribute("Name"));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
