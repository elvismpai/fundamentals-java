/**
 * Created by Elvis on 28/Sep/2015.
 */

import java.io.*;

public class AddFromFile {
    public static void main(String[] args) {
        String file = "C:/Users/DITLOU_DIKGOMO/Documents/1InfowareDocuments/JavaFundamentals/fundamentals-java/Java Exercises/src/data.txt";
        int sum = 0, count = 0;
        int[] numbers = new int[5];
        String output = "";

        try {

            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;

            while ((line = br.readLine()) != null) {
                numbers[count] = Integer.parseInt(line);
                count++;
            }
            br.close();

        } catch (IOException e) {
            System.out.println("ERROR: Couldn't open file " + file);
            e.printStackTrace();
        }
        for(int i = 0; i < 5; i++) {
            sum += numbers[i];
            output = numbers[i] + " ";

            if(i == numbers.length - 1) {
                System.out.print(output);
            }else{
                System.out.print(output + "+ ");
            }
        }

        System.out.println("= " + sum);
    }
}
