/**
 * Created by Elvis on 21/Sep/2015.
 */
import java.util.Scanner;

public class UserInput {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String Line;
        int Number = 0;

        System.out.print("Enter your name: ");
        Line = in.nextLine();
        System.out.print("Enter the number 75000: ");
        Number = in.nextInt();

        if (Number == 75000) {
            System.out.println("Your name is: " + Line);
            System.out.println("The number is: " + Number);
        }
        else {
            System.out.println("Error! Enter the correct number! ");
        }

    }

}


