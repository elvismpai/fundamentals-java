/**
 * Created by Elvis on 06/Oct/2015.
 */
import java.util.*;

public class BruteForceStringSearch {
    public static void main(String args[]){
        BruteForceStringSearch bfss = new BruteForceStringSearch();
        Scanner sc = new Scanner(System.in);
        String text = "Inforware Studios september internship";
        System.out.print("Enter the word you want to search: ");
        String pattern = sc.next();

        bfss.setString(text, pattern);
        int position = bfss.search();

        if(position != -1)
            System.out.println("The text: "+ pattern + " is found at position " + position);
        else
            System.out.print("The text: " + pattern + " - was not found");
    }
    char[] text, pattern;
    int intText, intPattern;

    public void setString(String textString,String patternString){
        text = textString.toCharArray();
        pattern = patternString.toCharArray();
        intText = textString.length();
        intPattern = patternString.length();
    }
    public int search() {
        for (int i = 0; i < intText - intPattern; i++) {
            int j = 0;
            while (j < intPattern && text[i+j] == pattern[j]) {
                j++;
            }
            if (j == intPattern) return i;
        }
        return -1;
    }
}
