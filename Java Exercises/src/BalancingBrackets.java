/**
 * Created by Elvis on 26/Sep/2015.
 */

import java.util.Random;
import java.util.Scanner;

public class BalancingBrackets {
    public static void main(String[] args) {
        BalancingBrackets objBB = new BalancingBrackets();
        Scanner loop = new Scanner(System.in);
        int numberOfBrackets;

        int openBrackets = 0, closeBrackets = 0;

        System.out.println("Bracket Balancing.");
        System.out.print("How many brackets do you want to check (Even number): ");
        numberOfBrackets = loop.nextInt();

        String randomBrackets = objBB.getStr(numberOfBrackets);
        if(numberOfBrackets % 2 == 0) {
            System.out.print(randomBrackets + " ");
        }
        else {
            System.out.println("ERROR: You have entered an ODD Number!");
            System.out.println("Results: " + randomBrackets + " NOT OK");
        }
        if(numberOfBrackets == 0)
            System.out.print("(empty) ");

       for(int i = 0; i < randomBrackets.length(); i++) {
            if (randomBrackets.charAt(i) == '[') {
                openBrackets++;
            }
            else if (randomBrackets.charAt(i) == ']')
                closeBrackets++;

        }
        if(numberOfBrackets % 2 == 0) {
            if (openBrackets > closeBrackets || closeBrackets > openBrackets)
                System.out.print("NOT OK");
            else
                System.out.print("OK");
        }
    }

    public static final char[] brackets = {'[', ']'};

    private static Random randomBrackets;

    public BalancingBrackets() {
        randomBrackets = new Random();
    }

    public static char getChar() {
        return brackets[randomBrackets.nextInt(brackets.length)];
    }

    public String getStr(int sz) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < sz; i++)
            sb.append(getChar());
        return new String(sb);
    }
}
